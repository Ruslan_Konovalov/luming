FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /src/requirements.txt /config/
WORKDIR /src
COPY ./ ./
RUN pip3 install -r /config/requirements.txt && apt-get clean