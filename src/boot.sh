#!/usr/bin/env bash
cd /src
echo "yes" | python manage.py collectstatic
gunicorn -w 3 --chdir ./ main.wsgi --bind 0.0.0.0:9000