from django.contrib import admin

from app.models import CommonSettings, Products, Filters, Images, SaleAside, SaleMain


admin.site.register(CommonSettings)
admin.site.register(Products)
admin.site.register(Filters)
admin.site.register(Images)
admin.site.register(SaleAside)
admin.site.register(SaleMain)