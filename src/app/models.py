from django.db import models
from django import forms


class CommonSettings(models.Model):
    """Model for settings"""

    class Meta:
        verbose_name_plural = 'common settings'

    phone_number = models.CharField(max_length=20, blank=True)
    yandex_counter = models.CharField(max_length=10000, blank=True)
    mail = models.EmailField(max_length=50, blank=True)


class Products(models.Model):
    class Meta:
        verbose_name_plural = 'products'

    name = models.CharField(max_length=1000, blank=True)
    price = models.CharField(max_length=1000, default=1000)
    text = models.CharField(max_length=20000)

    def __str__(self):
        return self.name


class Filters(models.Model):
    class Meta:
        verbose_name_plural = 'filters'

    product = models.ManyToManyField(Products, related_name='product_filters', blank=True)
    filter_name = models.CharField(max_length=100)

    def __str__(self):
        return self.filter_name


class Images(models.Model):
    class Meta:
        verbose_name_plural = 'images'

    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images')

    def __str__(self):
        return self.product.name


class SaleAside(models.Model):
    class Meta:
        verbose_name_plural = 'sale_aside'

    name = models.CharField(max_length=1000, blank=True)
    image = models.ImageField(upload_to='sale_aside')
    check = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class SaleMain(models.Model):
    class Meta:
        verbose_name_plural = 'sale_main'

    name = models.CharField(max_length=1000, blank=True)
    image = models.ImageField(upload_to='sale_main')
    check = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class CallForm(forms.Form):
    name = forms.CharField()
    phone = forms.CharField()
    address = forms.CharField()
