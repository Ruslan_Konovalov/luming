from django.core.mail import send_mail
from django.shortcuts import render
from itertools import chain
from django.template.loader import render_to_string
from django.http import HttpResponse, JsonResponse, BadHeaderError, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from app.models import CommonSettings, Filters, Products, Images, CallForm, SaleAside, SaleMain
from django.template import loader
from django.template import Context, Template

from main import settings


@csrf_exempt
def main(request):
    """Render home page"""
    try:
        yandex_counter = CommonSettings.objects.all()[0].yandex_counter
        phone_number = CommonSettings.objects.all()[0].phone_number
        mail = CommonSettings.objects.all()[0].mail
    except:
        yandex_counter, phone_number, mail = '', '', ''
    product_new = []
    filters_names = Filters.objects.all()
    products = Products.objects.all()
    saleaside = SaleAside.objects.filter(check=True)
    salemain = SaleMain.objects.filter(check=True)
    params = {
        'yandex_counter': yandex_counter,
        'phone_number': phone_number,
        'mail': mail,
        'filters_names': filters_names,
        'products': products,
        'saleaside': saleaside,
        'salemain': salemain,
    }
    return render(request, 'app/index.html', params)


@csrf_exempt
def update_filters(request):
    ids_ = request.POST.get('ids_').split(',')
    salemain = SaleMain.objects.filter(check=True)
    if not ids_[0]:
        res = Filters.objects.all()
        new_qs = Products.objects.none()
        products = Products.objects.all()
        for i in res:
            new_qs = list(chain(new_qs, i.product.all()))
        params = {'products': products,
                  'salemain': salemain,
                  }
    else:
        res = Filters.objects.filter(id__in=ids_)
        new_qs = Products.objects.none()
        for i in res:
            new_qs = list(chain(new_qs, i.product.all()))
        params = {'products': new_qs,
                  'salemain': salemain,
                  }
    # return HttpResponse(params)

    html = render_to_string('app/products.html', params)
    return HttpResponse(html)

    # t = loader.get_template('app/products.html')
    # return HttpResponse(t.render(params, request))


EMAIL_TO = ['2408199898@mail.ru']


@csrf_exempt
def callView(request):
    # Если форма заполнена корректно, сохраняем все введённые пользователем значения
    if request.method == 'POST':
        form = CallForm(request.POST)
        # Если форма заполнена корректно, сохраняем все введённые пользователем значения
        if form.is_valid():
            name = form.cleaned_data['name']
            phone = form.cleaned_data['phone']
            address = form.cleaned_data['address']

            try:
                message = 'Телефон: ' + phone + '   Имя: ' + name + '   Адресс: ' + address
                send_mail('Просьба перезвонить', message, settings.EMAIL_HOST_USER, EMAIL_TO)
            except BadHeaderError:  # Защита от уязвимости
                return HttpResponse('Invalid header found')
        else:
            # Заполняем форму
            form = CallForm()
    # Отправляем форму на страницу
    return HttpResponseRedirect("/")
