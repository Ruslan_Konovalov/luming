const cards = document.querySelectorAll('.card');

cards.forEach((card) => {
  const photoQuantity = card.querySelectorAll('.card__images img').length;
  const cardControl = card.querySelector('.card__control');
  const cardPhotos = card.querySelectorAll('.card__images img');
  const cardPhotosContainer = card.querySelector('.card__images');


  for (let i = 0; i < photoQuantity; i++) {
    cardControl.innerHTML += `<div data-index="${i}"></div>`;
    cardPhotos[i].setAttribute('data-index', i);
  }

  // cardControl.firstChild.classList.add('active'); # подсвечивает кружок зеленым


  const cardControlContainer = document.createElement('div');
  cardControlContainer.classList.add('card__control_container');
  cardPhotosContainer.append(cardControlContainer);

  for (let i = 0; i < photoQuantity; i++) {
    cardControlContainer.innerHTML += `
      <div style="width: ${100 / photoQuantity}%" class="card__photo_control" data-index="${i}"></div>
    `;
  }
});


cards.forEach((card) => {
  let index = 0;

  const cardControls = card.querySelectorAll('.card__control div');
  const cardPhotos = card.querySelectorAll('.card__images img');

  cardControls.forEach((control) => {
    control.addEventListener('mouseover', (e) => {
      index = control.dataset.index;

      cardPhotos.forEach((photo) => {
        photo.style.left = `calc(-100% * ${index})`;
      });

      cardControls.forEach((control) => {
        control.classList.remove('active');
      });

      control.classList.add('active');
    });
  });

  cardPhotos.forEach((photo) => {

    const hammertime = new Hammer(photo, { threshold: 3, velocity: 0 });
    hammertime.on('swipeleft', function (e) {
      if (index < cardPhotos.length - 1) {
        index++;
        cardPhotos.forEach((photo) => {
          photo.style.left = `calc(-100% * ${index})`;
        });

        cardControls.forEach((control) => {
          control.classList.remove('active');
        });

        cardControls[index].classList.add('active');
      }
    });

    hammertime.on('swiperight', function (e) {
      if (index > 0) {
        index--;
        cardPhotos.forEach((photo) => {
          photo.style.left = `calc(-100% * ${index})`;
        });

        cardControls.forEach((control) => {
          control.classList.remove('active');
        });

        cardControls[index].classList.add('active');
      }
    });
  });


  const cardPhotosContainer = card.querySelector('.card__images');
  const cardLastPhoto = cardPhotosContainer.querySelector('.card__last_photo');
  const cardNextPhoto = cardPhotosContainer.querySelector('.card__next_photo');
  const cardControlPhoto = card.querySelectorAll('.card__control_container div');

  cardControlPhoto.forEach((item) => {
    item.addEventListener('mouseover', (e) => {
      const index = item.getAttribute('data-index');

      cardPhotos.forEach((photo) => {
        photo.style.left = `calc(-100% * ${index})`;
      });

      cardControls.forEach((control) => {
        control.classList.remove('active');
      });

      cardControls[index].classList.add('active');
    });
  });


  const cardMorePhotos = card.querySelector('.card__more');
  const galery = document.querySelector('#galery');
  const overlay = galery.querySelector('.overlay');
  const content = galery.querySelector('.content');
  const photos = content.querySelector('.photos');
  const nextBtn = document.querySelector('#galery__next_btn');
  const lastBtn = content.querySelector('#galery__last_btn');


  nextBtn.addEventListener('click', (e) => {
    if (index < cardPhotos.length - 1) {
      index++;
      photos.querySelectorAll('img').forEach((photo) => {
        photo.style.left = `calc(-100% * ${index})`;
      });
    }
  });

  lastBtn.addEventListener('click', (e) => {
    if (index > 0) {
      index--;
      photos.querySelectorAll('img').forEach((photo) => {
        photo.style.left = `calc(-100% * ${index})`;
      });
    }
  });

  cardMorePhotos.addEventListener('click', (e) => {
    galery.style.display = 'flex';
    document.body.style.overflow = 'hidden';
    photos.innerHTML += card.querySelector('.card__images').innerHTML;

    content.querySelectorAll('img').forEach((photo) => {
      const index = photo.getAttribute('data-index');
      photo.style.left = `calc(100% * ${index})`;
    });
  });

  overlay.addEventListener('click', (e) => {
    galery.style.display = 'none';
    index = 0;
    document.body.style.overflow = 'auto';
    photos.innerHTML = '';
  });
});



// Sidebar

const sidebarBtnMobile = document.querySelector('#sidebar__btn_mobile');
const grid = document.querySelector('.grid');
const sidebarList = document.querySelector('.sidebar__list');
const sidebarBtn = document.querySelector('#sidebar__btn');
const sidebar = document.querySelector('#sidebar');
const sidebarOverlay = document.querySelector('.sidebar__overlay');

sidebarBtn.addEventListener('click', (e) => {
  if (grid.style.gridTemplateColumns === '75% 90%' && grid.style.overflow === 'hidden') {
    grid.style.gridTemplateColumns = '10% 90%';
    grid.style.overflow = 'auto';
    sidebarList.style.display = 'none';
    sidebarBtnMobile.style.display = 'block';
    sidebarBtn.style.display = 'none';
    sidebarOverlay.style.display = 'block';
  }
});

sidebarOverlay.addEventListener('click', (e) => {
  grid.style.gridTemplateColumns = '75% 90%';
  grid.style.overflow = 'hidden';
  sidebarList.style.display = 'block';
  sidebarList.style.height = 'auto';
  sidebarBtnMobile.style.display = 'none';
  sidebarBtn.style.display = 'flex';
  sidebarOverlay.style.display = 'none';
});

const main = document.querySelector('#main');
main.addEventListener('click', (e) => {
  if (grid.style.gridTemplateColumns === '75% 90%' && grid.style.overflow === 'hidden') {
    grid.style.gridTemplateColumns = '10% 90%';
    grid.style.overflow = 'auto';
    sidebarList.style.display = 'none';
    sidebarBtnMobile.style.display = 'block';
    sidebarBtn.style.display = 'none';
  }
});